﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileMonitoring.Interfaces;

namespace FileMonitoring
{
	public class FileMonitor : IFileMonitor
	{
		/// <summary>
		/// Путь к папке, за файлами в которой нужно следить
		/// </summary>
		private string Path { get; }

		/// <summary>
		/// Путь к папке, в которой должны сохраняться бэкапы
		/// </summary>
		private string BackupPath { get; }

		private readonly FileSystemWatcher fileWatcher;

		/// <summary>
		/// Состояния папки Path на все события, произошедшие с файлами. Ключ - время в секундах состояния папки,
		/// значение - имена файлов в папке на помент события через пробел
		/// </summary>
		private readonly Dictionary<long, StringBuilder> filesToTime = new Dictionary<long, StringBuilder>();

		private bool disposed = false;

		public FileMonitor(IConfiguration configuration)
		{
			Path = configuration.Path;
			BackupPath = configuration.BackupPath;

			DirectoryInfo dirBackUp = new DirectoryInfo(BackupPath);
			dirBackUp.Create();

			DirectoryInfo dirInfo = new DirectoryInfo(Path);
			fileWatcher = new FileSystemWatcher(dirInfo.FullName);

			fileWatcher.Created += FileWatcherEvent;
			fileWatcher.Deleted += FileWatcherEvent;
			fileWatcher.Changed += FileWatcherEvent;
			fileWatcher.Renamed += FileWatcherEvent;
		}

		public void Start()
		{
			addFilesNames(); // Создаем состояние папки по типу ключ - текущее время, значение имена файлов в паке в это время.
							 // + копируем файлы из папки path в папку BackupPath (если файлы есть).

			fileWatcher.EnableRaisingEvents = true; // Начало слежки за файлами.
		}

		public void Stop()
		{
			fileWatcher.EnableRaisingEvents = false;
			var dirToEvents = new DirectoryInfo(BackupPath);
			dirToEvents.Delete(true);
		}

		public void Reset(DateTime onDateTime)
		{
			fileWatcher.EnableRaisingEvents = false;

			#region GetFolderStateToBackupTime

			List<long> keysToResetTime = new List<long>(); // Список ключей состояний отслеживаемой папки на время до или включая переданное время.

			foreach (var time in filesToTime)
			{
				if (time.Key < onDateTime.Ticks || time.Key == onDateTime.Ticks)
				{
					keysToResetTime.Add(time.Key);
				}
			}

			var sortedKeysToResetTime = keysToResetTime.OrderByDescending(u => u);

			long keyFolderStateToResetTime = sortedKeysToResetTime.First(); // Получаем ключ состояния папки на момент на который идет откат

			string filesInFolderToResetTime = ""; // Получаем имена файлов которые находились в папке на в тот момент на который идет откат

			foreach (var listOfFiles in filesToTime)
			{
				if (listOfFiles.Key == keyFolderStateToResetTime)
				{
					filesInFolderToResetTime = listOfFiles.Value.ToString();
				}
			}

			string[] filesNames = filesInFolderToResetTime.Split(' '); // Список названий файлов в момент на который идет восстановление

            #endregion

			if(filesNames.Length == 0) // Проверяем были ли на момент, который идет восстановление файлы в папке, если нет, то очищаем папку и считаем ее восстановленной.
            {
				string[] filesInPath = Directory.GetFiles(Path);

				foreach(var filePath in filesInPath)
                {
					var file = new FileInfo(filePath);
                    file.Delete();
                }

				return;
            }

            #region GetPathsToAllFilesInBackUpPath

            List<string> pathsFilesInBackupFolder = getAllFilesInFolder(BackupPath); // Получаем список путей ко всем файлам в папке с бекапами.

			List<string> recoveryFileSelectionList = new List<string>(); // Пути к файлам в папке с бекапами, имена которых совпадают с именами файлов в отслеживаемой
																		 // папке во время на которое восстанавливается состояние папки.
			foreach (var pathFileInBackupFoler in pathsFilesInBackupFolder)
			{
				if(filesNames.Length > 0)
                {
					FileInfo fileInBackupFolder = new FileInfo(pathFileInBackupFoler);
					string fileNameWithoutTicks = fileInBackupFolder.Name.Remove(0, 19);

					foreach (var fileNameInResetTime in filesNames)
					{
						if (fileNameWithoutTicks == fileNameInResetTime)
						{
							recoveryFileSelectionList.Add(pathFileInBackupFoler);
						}
					}
				}
			}

            #endregion

            #region GetStateFilesInBackupFolderToAllTime

            List<string>[] filesStates = new List<string>[filesNames.Length];	  // Список состояний каждого файла в отслеживаемой папке 
																				  // взятый из списка файлов в папке с бекапами.

			for (int i = 0; i < filesStates.Length; i++)
			{
				filesStates[i] = new List<string> { };

				foreach (var file in recoveryFileSelectionList)
				{
					FileInfo fileInBackupFolder = new FileInfo(file);
					string fileNameWithoutTicks = fileInBackupFolder.Name.Remove(0, 19);

					if (filesNames[i] == fileNameWithoutTicks)
					{
						filesStates[i].Add(file);
					}

				}

			}

            #endregion

            #region GetStatesFilesInBackupfolderOfStart-Stop

            List<string>[] filesStatesToSelect = new List<string>[filesStates.Length]; // Состояния нужных файлов на время до времени на которое идет восстановление
																					   // или во время на которое идет восстановление.

			for (int i = 0; i < filesStatesToSelect.Length; i++)
			{
				filesStatesToSelect[i] = new List<string>();
				foreach (var fileState in filesStates[i])
				{
					if (fileState != null)
					{
						FileInfo fileInBackupFolder = new FileInfo(fileState);
						long fileTicks = long.Parse(fileInBackupFolder.Name.Remove(18));

						if (fileTicks < onDateTime.Ticks || fileTicks == onDateTime.Ticks)
						{
							filesStatesToSelect[i].Add(fileState);
						}
					}
				}
			}

			#endregion

			#region GetStateFilesToResetTime

			List<string> filesToResetTime = new List<string>(); // Состояния файлов на время на которое идет восстановление

			foreach (var fileStatesToSelect in filesStatesToSelect)
			{
				List<long> timesSates = new List<long>();

				foreach (var fileStateToSelect in fileStatesToSelect)
				{
					FileInfo fileInBackupFolder = new FileInfo(fileStateToSelect);
					long fileTicks = long.Parse(fileInBackupFolder.Name.Remove(18));
					timesSates.Add(fileTicks);
				}

				long timeSate = timesSates.OrderByDescending(i => i).First();

				foreach (var fileStateToSelect in fileStatesToSelect)
				{
					FileInfo fileInBackupFolder = new FileInfo(fileStateToSelect);
					long fileTicks = long.Parse(fileInBackupFolder.Name.Remove(18));
					if (fileTicks == timeSate)
					{
						filesToResetTime.Add(fileStateToSelect);
					}

				}
			}

            #endregion

            #region ResetFiles

            string[] filesToRemoveInPath = Directory.GetFiles(Path);
			foreach (var fileToRemovePath in filesToRemoveInPath)
			{
				FileInfo fileToRemove = new FileInfo(fileToRemovePath);
				string fileToRemoveName = fileToRemove.Name;
				string fileToRemoveFormat = fileToRemoveName.Remove(0, fileToRemoveName.Length - 3);
				
				if(fileToRemoveFormat == "txt")
					fileToRemove.Delete();
			}


			foreach (var filePathToResetTime in filesToResetTime)
			{
				FileInfo fileToReset = new FileInfo(filePathToResetTime);

				string fileNameWithoutTicks = fileToReset.Name.Remove(0, 19);
				string fileToResetFormat = fileNameWithoutTicks.Remove(0, fileNameWithoutTicks.Length - 3);

				if(fileToResetFormat == "txt")
					fileToReset.CopyTo(System.IO.Path.Combine(Path, fileNameWithoutTicks));
			}

			#endregion

			fileWatcher.EnableRaisingEvents = true;

			addFilesNames();

		}

        public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					fileWatcher.EnableRaisingEvents = false; // Останавливаем слежку за файлами.
					var dirToEvents = new DirectoryInfo(BackupPath);
					dirToEvents.Delete(true);
				}
				disposed = true;
			}
		}

		~FileMonitor()
		{
			Dispose(false);
		}

		#region MethodsToEvents

		private void FileWatcherEvent(object sender, FileSystemEventArgs fileSystemEventArgs)
		{
            string fileName = fileSystemEventArgs.Name;
			string fileFormat = fileName.Remove(0, fileName.Length - 3);

			if(fileFormat == "txt")
				addFilesNames(); // Создаем состояние папки по типу ключ - текущее время, значение имена файлов в паке в это время.
							     // + копируем файлы из папки path в папку BackupPath (если файлы есть).
		}

		#endregion

		#region  OtherMethods

		private static List<string> getAllFilesInFolder(string path)
		{
			string[] files = Directory.GetFiles(path);

			List<string> pathsFilesInFolder = new List<string>();

			foreach (string file in files)
			{
				pathsFilesInFolder.Add(file);
			}
			return pathsFilesInFolder;
		}

		private void addFilesNames()
		{
			string[] files = Directory.GetFiles(Path); // Все файлы в дирректории Path (только дирректория Path, папки внутри не смотрит).

			long tickOnEvent = DateTime.Now.Ticks; // Узнаем текущее время, чтобы в дальнейшем ассоциировать данное состояние с этим временем.
			filesToTime.Add(tickOnEvent, new StringBuilder());
			if (files.Length > 0)
            {
				foreach (var file in files)
				{
					FileInfo fileInf = new FileInfo(file);

					if (fileInf.Exists)
                    {
						fileInf.CopyTo(System.IO.Path.Combine(BackupPath, tickOnEvent + " " + fileInf.Name), true);

						if (file != files.Last())
						{
							filesToTime[tickOnEvent].Append(fileInf.Name + " ");
						}
						else
						{
							filesToTime[tickOnEvent].Append(fileInf.Name);
						}
					}
				}
			}		
		}

        #endregion
    }
}
