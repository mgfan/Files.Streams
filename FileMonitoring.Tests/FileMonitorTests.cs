using System;
using System.IO;
using System.Threading.Tasks;
using FileMonitoring.Interfaces;
using Moq;
using NUnit.Framework;

namespace FileMonitoring.Tests
{
	[TestFixture]
	public class FileMonitorTests
	{
		private const string BackupPath = "_backups";

		private Mock<IConfiguration> _configurationMock;
		private static readonly string Path = System.IO.Path.Combine(TestContext.CurrentContext.TestDirectory, "Files");
		private IConfiguration Configuration => _configurationMock.Object;
		
		[OneTimeSetUp]
		public void Setup()
		{
			_configurationMock = new Mock<IConfiguration>();
			_configurationMock.Setup(c => c.BackupPath).Returns(BackupPath);
			_configurationMock.Setup(c => c.Path).Returns(Path);
		}

		[Test]
		public async Task Start_ModifyTextFile_FileContainsOriginalContentAfterReset()
		{
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");
				var initialFile1Content = await File.ReadAllTextAsync(file1Path);

				var dateTimeToRestore = DateTime.Now;
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);

				monitor.Reset(dateTimeToRestore);
				var restoredFile1Content = await File.ReadAllTextAsync(file1Path);

				Assert.AreEqual(initialFile1Content, restoredFile1Content);
			}
		}

		[Test]
		public async Task Start_ModifyTextFile_FileDoesNotContainModifiedContentAfterReset()
		{
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");
				var initialFile1Content = await File.ReadAllTextAsync(file1Path);
				await Task.Delay(1000);

				var dateTimeToRestore = DateTime.Now;
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);

				var modifiedContent = await File.ReadAllTextAsync(file1Path);
				monitor.Reset(dateTimeToRestore);

				Assert.AreNotEqual(initialFile1Content, modifiedContent);
			}
		}

		[Test]
		public async Task Start_ModifyAnotherFile_FileIsModifiedAfterReset()
		{
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				var logFilePath = System.IO.Path.Combine(Path, "NewFile3.log");
				var initialLogFileContent = await File.ReadAllTextAsync(logFilePath);
				await Task.Delay(1000);

				var dateTimeToRestore = DateTime.Now;
				await File.AppendAllLinesAsync(logFilePath, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);

				monitor.Reset(dateTimeToRestore);
				var contentAfterRestore = await File.ReadAllTextAsync(logFilePath);

				Assert.AreNotEqual(initialLogFileContent, contentAfterRestore);
			}
		}

		[Test]
		public async Task Start_ModifyTextFileSomeTimes_FileContainsExpectedContentAfterReset()
		{
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");
				var initialFile1Content = await File.ReadAllTextAsync(file1Path);

				var dateTimeToRestore1 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);
				var file1Content1 = await File.ReadAllTextAsync(file1Path);

				var dateTimeToRestore2 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);
				var file1Content2 = await File.ReadAllTextAsync(file1Path);

				var dateTimeToRestore3 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);

				monitor.Reset(dateTimeToRestore1);
				var restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				Assert.AreEqual(initialFile1Content, restoredFile1Content);

				monitor.Reset(dateTimeToRestore3);
				restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				Assert.AreEqual(file1Content2, restoredFile1Content);

				monitor.Reset(dateTimeToRestore2);
				restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				Assert.AreEqual(file1Content1, restoredFile1Content);
			}
		}

		[Test]
		public async Task Start_ModifySomeTextFilesSomeTimes_FilesContainExpectedContentAfterReset()
		{
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");
				var initialFile1Content = await File.ReadAllTextAsync(file1Path);
				var file2Path = System.IO.Path.Combine(Path, "NewFile2.txt");
				var initialFile2Content = await File.ReadAllTextAsync(file2Path);

				var dateTimeToRestore1 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await File.AppendAllLinesAsync(file2Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);
				var file1Content1 = await File.ReadAllTextAsync(file1Path);
				var file2Content1 = await File.ReadAllTextAsync(file2Path);

				var dateTimeToRestore2 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);
				var file1Content2 = await File.ReadAllTextAsync(file1Path);
				var file2Content2 = await File.ReadAllTextAsync(file2Path);

				var dateTimeToRestore3 = DateTime.Now;
				await Task.Delay(1000);
				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await File.AppendAllLinesAsync(file2Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);

				monitor.Reset(dateTimeToRestore1);
				var restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				var restoredFile2Content = await File.ReadAllTextAsync(file2Path);
				Assert.AreEqual(initialFile1Content, restoredFile1Content);
				Assert.AreEqual(initialFile2Content, restoredFile2Content);

				monitor.Reset(dateTimeToRestore3);
				restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				restoredFile2Content = await File.ReadAllTextAsync(file2Path);
				Assert.AreEqual(file1Content2, restoredFile1Content);
				Assert.AreEqual(file2Content2, restoredFile2Content);

				monitor.Reset(dateTimeToRestore2);
				restoredFile1Content = await File.ReadAllTextAsync(file1Path);
				restoredFile2Content = await File.ReadAllTextAsync(file2Path);
				Assert.AreEqual(file1Content1, restoredFile1Content);
				Assert.AreEqual(file2Content1, restoredFile2Content);
			}
		}

		[Test]
		public async Task Start_ModifyTextFile_ThereIsNotBackupDirectoryAfterDispose()
		{
			var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");
			using (var monitor = GetFileMonitor())
			{
				monitor.Start();

				await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
				await Task.Delay(1000);
			}

			await File.AppendAllLinesAsync(file1Path, new []{Guid.Empty.ToString()});
			Assert.IsFalse(Directory.Exists(Configuration.BackupPath));
		}

		[Test]
		public async Task Start_ModifyTextFile_ThereIsNotBackupDirectoryAfterStop()
		{
			var monitor = GetFileMonitor();
			monitor.Start();

			var file1Path = System.IO.Path.Combine(Path, "NewFile1.txt");

			await File.AppendAllLinesAsync(file1Path, new[] {Guid.Empty.ToString()});
			await Task.Delay(1000);
			monitor.Stop();

			await File.AppendAllLinesAsync(file1Path, new[] {Guid.Empty.ToString()});
			Assert.IsFalse(Directory.Exists(Configuration.BackupPath));
		}

		private FileMonitor GetFileMonitor()
		{
			return new FileMonitor(Configuration);
		}
	}
}
